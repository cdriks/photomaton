const fs = require('fs');
const Webcam = require('webcamjs')
const Swal = require('sweetalert2')

Swal.fire('Hello world!')

// var screen_width = $(window).width();
// var screen_height = $(window).height();
//
let webcamHeight = 600;
let webcamWidth = 800;

// var webcamDisplayWidth = Math.min( screen_width * 0.9, (screen_height - height_bottom ) / webcamHeight * webcamWidth * 0.9);
// var webcamDisplayHeight = Math.min( screen_height * 0.9 - height_bottom, screen_width / webcamWidth * webcamHeight * 0.9);
// $('.webcam').width(webcam_display_width);
//

Webcam.set({
  // // live preview size
  width: document.querySelector("main").clientWidth,
  height: document.querySelector("main").clientHeight,
  //
  // // device capture size
  // dest_width: webcamWidth,
  // dest_height: webcamHeight,
  image_format: 'jpeg',
  jpeg_quality: 90,
  constraints: {
    width: { exact: webcamWidth },
    height: { exact: webcamHeight }
  }
});
Webcam.attach( '#webcam' );

Webcam.on( 'live', function() {
  document.querySelector("#snapshot").style.display = "inline";
  // $('.Info').width($('#webcam').width());
} );



function delaysnapshot(i){
  Webcam.unfreeze()
  // document.querySelector("#snapshot").classList.add('btn-disabled btn-default').removeClass('btn-primary').html('<i class="fa fa-spinner fa-spin fa-fw"></i> Photo en cours').attr("disabled", "disabled");
  document.querySelector("#Info").style.display = "block";
  // $("#Info").addClass("alert alert-info").removeClass("alert alert-success");
  document.querySelector("#Info").innerHTML = "<i class='fa fa-camera fa-3x' aria-hidden='true'></i><br /><h1>"+ i + "</h1>"
  if(i==0){
    snapshot();

  }
  else{
    window.setTimeout(function(){  document.querySelector("#Info").style.display = "none"},500);
    window.setTimeout(function(){delaysnapshot(i-1)},1000);
  }

}

function snapshot(){

  Webcam.snap( function(data_uri) {
    Webcam.freeze();
    var path_images = "./img/";

    var data = data_uri.replace(/^data:image\/\w+;base64,/, "");
    var buf = new Buffer(data, 'base64');

    // //Generating Image
    image_file = 'PHOTOmaton_'+get_date()+'.jpg';
    fs.writeFile(path_images + image_file, buf,function(err){
      if (err) throw err;
        window.setTimeout(function(){Webcam.unfreeze()},4000);
    });

  } );

}

function get_date(){
  var d = new Date;
  return d.getFullYear().toString() + "-" + (d.getMonth()+1).toString() + "-" + d.getDate().toString() + "_" + d.getHours().toString() + "-" + d.getMinutes().toString() + "-" + d.getSeconds().toString()
}
