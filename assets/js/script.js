const fs = require('fs');
const Swal = require('sweetalert2')
const path = require('path');
const delay = 5
const gifFrameDelay = 1000
document.querySelector("#snapshot").onclick = () => {
  delaysnapshot(delay)
}

const GIFEncoder = require('gifencoder');
// const pngFileStream = require('png-file-stream');
let encoder



const webcamElement = document.querySelector('#webcam');
const canvas = document.createElement('canvas');


vendorUrl = window.URL || window.webkitURL;
if (navigator.mediaDevices.getUserMedia) {
  navigator.mediaDevices.getUserMedia({
      video: {
        width: {
          min: 1024,
          ideal: 1920,
          max: 1920
        },
        height: {
          min: 576,
          ideal: 1080,
          max: 1080
        },
      }
    })
    .then(function (stream) {
      webcamElement.srcObject = stream;

      streamWidth = stream.getVideoTracks()[0].getSettings().width
      streamHeight = stream.getVideoTracks()[0].getSettings().height

      let aspectRatio = streamWidth / streamHeight

      if (document.querySelector("main").clientWidth / aspectRatio > document.querySelector("main").clientHeight) {
        webcamElement.width = document.querySelector("main").clientHeight * aspectRatio
      } else {
        webcamElement.height = document.querySelector("main").clientWidth / aspectRatio
      }

      canvas.width = streamWidth;
      canvas.height = streamHeight;

      newEncoder()

      document.querySelector("#snapshot").style.display = "inline";
    }).catch(function (error) {
      console.log("Something went wrong!" + error);
    });
}



function delaysnapshot(timer, numberPhoto = undefined) {

  if (numberPhoto == undefined) {
    numberPhoto = 1
  }
  document.querySelector('#info div').innerHTML = "<i class='fa fa-camera fa-3x' aria-hidden='true'></i><br />Photo " + numberPhoto + "/" + document.body.dataset.numberPhotos + "<br/><h1>" + timer + "</h1>"

  document.querySelector('#info').style.display = "block";

  if (timer == 0) {
    snapshot(() => {
      if (numberPhoto >= document.body.dataset.numberPhotos) {
        if (document.body.dataset.numberPhotos > 1) {
          const path_images = path.join(__dirname, "/img/");

          let gifName = path.join(path_images, 'PHOTOmaton_' + get_date() + '.gif')
          encoder.finish();

          let buf = encoder.out.getData();

          newEncoder()

          fs.writeFile(gifName, buf, err => {

            Swal.fire({
              imageUrl: gifName,
              width: "90%",
              showConfirmButton: false,
              timer: numberPhoto * gifFrameDelay * 2
            })


          });



        }

        updateNumberPhotos()
      } else {
        delaysnapshot(delay, numberPhoto + 1)
      }

    })

  } else {
    setTimeout(() => {
      document.querySelector('#info').style.display = "none"
    }, 500);
    setTimeout(() => {
      delaysnapshot(timer - 1, numberPhoto)
    }, 1000);
  }

}

function snapshot(callback = () => {}) {

  var ctx = canvas.getContext('2d');
  //draw image to canvas. scale to target dimensions
  ctx.drawImage(webcamElement, 0, 0, canvas.width, canvas.height);


  if (document.body.dataset.numberPhotos > 1) {
    encoder.addFrame(ctx);

  }

  //convert to desired file format
  var dataURI = canvas.toDataURL('image/jpeg'); // can also use 'image/png'
  const path_images = path.join(__dirname, "/img/");

  let data = dataURI.replace(/^data:image\/\w+;base64,/, "");
  let buf = new Buffer(data, 'base64');

  let imageFile = path.join(path_images, 'PHOTOmaton_' + get_date() + '.jpg')

  if (!fs.existsSync(path_images)) {
    fs.mkdirSync(path_images)
  }

  fs.writeFile(imageFile, buf, function (err) {

    if (err) throw err;

    document.querySelector('#info').style.display = "none"

    if (document.body.dataset.numberPhotos <= 1) {

      Swal.fire({
        imageUrl: imageFile,
        width: "90%",
        showConfirmButton: false,
        timer: 2000,
        onClose: callback
      })
    } else {
      callback()
    }

  });
}

function get_date() {
  var d = new Date;
  return d.getFullYear().toString() + "-" + (d.getMonth() + 1).toString() + "-" + d.getDate().toString() + "_" + d.getHours().toString() + "-" + d.getMinutes().toString() + "-" + d.getSeconds().toString()
}

function updateNumberPhotos(button = undefined) {
  let numberPhotos
  if (button == undefined) {
    numberPhotos = 1
  } else {
    numberPhotos = parseFloat(document.body.dataset.numberPhotos);
    numberPhotos += parseFloat(button.dataset.value)
    numberPhotos = Math.max(numberPhotos, 1)
    numberPhotos = Math.min(numberPhotos, 10)
  }

  document.body.dataset.numberPhotos = numberPhotos

  document.querySelector("#snapshot span").innerHTML = "Prendre " + numberPhotos + " photo" + (numberPhotos > 1 ? "s" : "")

}


function newEncoder() {
  encoder = new GIFEncoder(streamWidth, streamHeight);
  encoder.start();
  encoder.setRepeat(0); // 0 for repeat, -1 for no-repeat
  encoder.setDelay(gifFrameDelay); // frame delay in ms
  encoder.setQuality(10); // image quality. 10 is default.
}
